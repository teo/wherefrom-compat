# Revision history for wherefrom-compat

## 0.2.0.0 -- 2024/05/24

* Migrate to new base-4.20 version of the InfoProv API.

## 0.1.1.1 -- 2024/05/24

* Support GHC-9.10

## 0.1.1.0 -- 2024/02/01

* Fix interface around srcFile/srcSpan.

## 0.1.0.0 -- 2024/02/01

* First version. Released on an unsuspecting world.
